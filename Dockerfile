FROM nginx:stable-alpine
LABEL MAINTAINER="yuki@coala.io"

COPY ./docker /

CMD nginx -g "daemon off;"
